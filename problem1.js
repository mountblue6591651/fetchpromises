// Perform a fetch request to the specified URL
const users = fetch('https://jsonplaceholder.typicode.com/users');

// Handle the response using the promise syntax
users
  .then((response) => {
    // Parse the JSON in the response body and return the resulting promise
    return response.json();
  })
  .then((data) => {
    // Process the JSON data obtained from the previous promise
    console.log(data);
  })
  .catch((error) => {
    // Handle any errors that occurred during the fetch or JSON parsing
    console.error(error);
  });
