// Perform a fetch request to the specified URL for users
const users = fetch('https://jsonplaceholder.typicode.com/users');

// Perform a fetch request to the specified URL for todos
const todos = fetch('https://jsonplaceholder.typicode.com/todos');

// Handle the response for users using the promise syntax
users
  .then((response) => {
    // Parse the JSON in the response body and return the resulting promise
    return response.json();
  })
  .then((userData) => {
    // Process and log the JSON data obtained from the users response
    console.log('Users:', userData);
    
    // Return the promise for the todos fetch to continue the chain
    return todos;
  })
  .then((response) => {
    // Parse the JSON in the response body for todos and return the resulting promise
    return response.json();
  })
  .then((todosData) => {
    // Process and log the JSON data obtained from the todos response
    console.log('Todos:', todosData);
  })
  .catch((error) => {
    // Handle any errors that occurred during the fetch or JSON parsing
    console.error(error);
  });
