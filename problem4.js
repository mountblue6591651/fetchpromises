// Perform a fetch request to get a list of users
const users = fetch("https://jsonplaceholder.typicode.com/users");

// Handle the response for users using the promise syntax
users
  .then((response) => {
    // Parse the JSON in the response body and return the resulting promise
    return response.json();
  })
  .then((userData) => {
    // Extract user IDs from the user data
    let userIds = userData.map((user) => {
      return user.id;
    });

    // Iterate through each user ID and fetch additional details
    userIds.forEach(id => {
        // Perform a fetch request to get detailed information for a specific user
        let userDetailPromise = fetch(`https://jsonplaceholder.typicode.com/users?id=${id}`);
        userDetailPromise
            .then((response) => {
                // Parse the JSON in the response body and return the resulting promise
                return response.json();
            })
            .then((details) => {
                // Process and log the detailed information for the user
                console.log('User Details:', details);
            })
            .catch((error) => {
                // Handle any errors that occurred during the fetch or JSON parsing for user details
                console.error(error);
            });
    });

  })
  .catch((error) => {
    // Handle any errors that occurred during the fetch or JSON parsing for the initial user data
    console.error(error);
  });
