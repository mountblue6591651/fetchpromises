// Perform a fetch request to get a list of todos
const todosPromise = fetch("https://jsonplaceholder.typicode.com/todos");

// Handle the response for todos using the promise syntax
todosPromise
  .then((response) => {
    // Parse the JSON in the response body and return the resulting promise
    return response.json();
  })
  .then((todosData) => {
    // Extract the user ID from the first todo item
    let userId = todosData[0].userId;

    // Perform a fetch request to get detailed information for the user
    let userDetailPromise = fetch(
      `https://jsonplaceholder.typicode.com/users?id=${userId}`
    );
    userDetailPromise
      .then((response) => {
        // Parse the JSON in the response body and return the resulting promise
        return response.json();
      })
      .then((details) => {
        // Process and log the detailed information for the user
        console.log("User Details:", details);
      })
      .catch((error) => {
        // Handle any errors that occurred during the fetch or JSON parsing for user details
        console.error(error);
      });
  })
  .catch((error) => {
    // Handle any errors that occurred during the fetch or JSON parsing for the initial todos data
    console.error(error);
  });
